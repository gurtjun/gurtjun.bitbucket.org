function collapseNavbar() {
    if ($(".navbar").offset().top > 100) {
        $(".navbar-fixed-top").addClass("nav-scrolled");
    } else {
        $(".navbar-fixed-top").removeClass("nav-scrolled");
    }
}

function setToday() {
    var monthNames = ["januari", "februari", "maart", "april", "mei", "juni",
        "juli", "augustus", "september", "oktober", "november", "december"
    ];

    var d = new Date();
    document.getElementById('today').innerHTML = d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getUTCFullYear();
    console.log(d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getUTCFullYear())
}


$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar,setToday());

$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top-49
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

$('.navbar-collapse ul li a').click(function() {
    if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
        $('.navbar-toggle:visible').click();
    }
});