angular
    .module('mini-kosten', [])
    .controller('MainController', function ($scope) {
        $scope.bivResult = null;
        $scope.comparison = {};
        $scope.index = 0;
        $scope.inschrijvingsdatum = new Date();

        $scope.minis = {
            cooper: {
                name: "Cooper",
                slug: "cooper",
                gearbox: {
                    manual: {
                        name: "Manueel",
                        slug: "manual",
                        co2: 107,
                        fPK: 8,
                        kW: 100,
                        pk: 136,
                        verbruik: 4.6,
                        weight: 1160,
                        autonomie: 890,
                        price: 20260,
                        tax: 182.03
                    },
                    auto: {
                        name: "Automaat",
                        slug: "auto",
                        co2: 112,
                        fPK: 8,
                        kW: 100,
                        pk: 136,
                        verbruik: 4.8,
                        weight: 1190,
                        autonomie: 850,
                        price: 21989,
                        tax: 185.42
                    }
                }
            },
            cooper_s: {
                name: "Cooper S",
                slug: "cooper_s",
                gearbox: {
                    manual: {
                        name: "Manueel",
                        slug: "manual",
                        co2: 136,
                        fPK: 11,
                        kW: 141,
                        pk: 192,
                        verbruik: 5.8,
                        weight: 1235,
                        autonomie: 770,
                        price: 25460,
                        tax: 361.36
                    },
                    auto: {
                        name: "Automaat",
                        slug: "auto",
                        co2: 126,
                        fPK: 11,
                        kW: 141,
                        pk: 192,
                        verbruik: 5.4,
                        weight: 1250,
                        autonomie: 830,
                        price: 27189,
                        tax: 349.21
                    }
                }
            }
        };

        $scope.setMini = function (miniType) {
            $scope.selectedMini = $scope.minis[miniType];
            $scope.gearboxes = $scope.minis[miniType].gearbox

            if ($scope.selectedGearbox) {
                $scope.selectedGearbox = $scope.selectedMini.gearbox[$scope.selectedGearbox.slug];
            }
        };

        $scope.setGearbox = function (gearboxType) {
            $scope.selectedGearbox = $scope.selectedMini.gearbox[gearboxType];
        };

        $scope.calcBIV = function () {
            $scope.compareAdded = false;

            if ($scope.selectedGearbox)
                $scope.resultBIV = ((Math.pow(($scope.selectedGearbox.co2 + 18) / 246, 6) * 4500 + 20.61) * getAgeValue());
        };

        $scope.calcTaks = function () {
            var eco = Math.abs($scope.selectedGearbox.co2 - 122);


        };

        $scope.addCompare = function () {
            var compare = {};

            compare.name = $scope.selectedMini.name;
            compare.slug = $scope.selectedMini.slug;
            compare.gearbox = $scope.selectedGearbox.name;
            compare.biv = $scope.resultBIV;
            compare.tax = $scope.selectedGearbox.tax;
            compare.year = $scope.inverkeerstelling.getMonth()+1 + "/" + $scope.inverkeerstelling.getFullYear();

            $scope.comparison[$scope.index] = compare;
            $scope.index++;

            $scope.compareAdded = true;
        };

        function getAge() {
            var today = new Date($scope.inschrijvingsdatum);
            var make = new Date($scope.inverkeerstelling);
            var months;
            months = (today.getFullYear() - make.getFullYear()) * 12;
            months -= make.getMonth() + 1;
            months += today.getMonth();
            var diff_days = today.getDate() - make.getDate();
            if (diff_days <= 0 || diff_days < today.getDate()) {
                months++;
            }
            return months <= 0 ? 0 : months;
        }

        function getAgeValue() {
            var months = getAge();

            if (months < 12) {
                return 1;
            } else if (months <= 23) {
                return 0.9;
            } else if (months <= 35) {
                return 0.8;
            } else if (months <= 47) {
                return 0.7;
            } else if (months <= 59) {
                return 0.6;
            } else if (months <= 71) {
                return 0.5;
            } else if (months <= 83) {
                return 0.4;
            } else if (months <= 95) {
                return 0.3;
            } else if (months <= 107) {
                return 0.2;
            } else if (months > 107) {
                return 0.1;
            }
        }
    });